import Vue from 'vue'
import Vuex from 'vuex'
import createLogger from 'vuex/dist/logger'
// modules
import * as modules from './modules/'
console.log(modules)

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  strict: debug,
  state: {

  },
  mutations: {

  },
  actions: {

  },
  modules: {
    ...modules
  },
  plugins: debug ? [createLogger()] : []
})
