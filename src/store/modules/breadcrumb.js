import {
  SET_BREADCRUMB
} from '../mutation-types'

/**
 * state
 */
const state = {
  breadcrumb: false
}

/**
 * actions
 */
const actions = {
  setBreadcrumb ({ commit }, payload) {
    commit(SET_BREADCRUMB, payload)
  }
}

/**
 * mutations
 */
const mutations = {
  [SET_BREADCRUMB]: (state, breadcrumb) => {
    state.breadcrumb = breadcrumb
  }
}

/**
 * getters
 */
const getters = {
  breadcrumb: (state) => state.breadcrumb
}

export default {
  state,
  actions,
  mutations,
  getters
}
