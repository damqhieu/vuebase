import {
  SET_DATA_VIEW,
  UPDATE_DATA_VIEW_ENTRY
} from '../mutation-types'
import { get, set, findIndex } from 'lodash'

const DEFAULT_PAGINATION = {current_page: null, total_pages: null, total: null, count: null, per_page: null}
/**
 * state
 */
const state = {
  dataViews: {}
}

/**
 * actions
 */
const actions = {
  setDataview ({ commit }, payload) {
    commit(SET_DATA_VIEW, payload)
  },
  initDataview ({dispatch, state}, payload) {
    payload.name && !state.dataViews[payload.name] && dispatch('setDataview', {name: payload.name, data: [], pagination: {...DEFAULT_PAGINATION}, last_scroll_pos: 0})
  },
  reloadDataview ({ dispatch }, payload) {
    payload.name && dispatch('setDataview', {name: payload.name, data: [], pagination: {...DEFAULT_PAGINATION}, last_scroll_pos: 0})
  },
  updateDataviewEntry ({ commit }, payload) {
    commit(UPDATE_DATA_VIEW_ENTRY, payload)
  }
}

/**
 * mutations
 */
const mutations = {
  [SET_DATA_VIEW]: (state, payload) => {
    let { name, data, pagination, last_scroll_pos } = payload
    data && set(state.dataViews, `${name}.data`, data)
    pagination && set(state.dataViews, `${name}.pagination`, pagination)
    Number.isInteger(last_scroll_pos) && set(state.dataViews, `${name}.last_scroll_pos`, last_scroll_pos)
  },
  [UPDATE_DATA_VIEW_ENTRY]: (state, payload) => {
    let { name, data, key } = payload
    let index = findIndex(get(state.dataViews, `${name}.data`), {[key]: data[key]})
    console.log('before', get(state.dataViews, `${name}.data[${index}]`))
    index !== -1 && !!data && set(state.dataViews, `${name}.data[${index}]`, data)
  }
}

/**
 * getters
 */
const getters = {
  dataViews: state => name => get(state.dataViews, name, {data: [], pagination: {...DEFAULT_PAGINATION}, last_scroll_pos: 0})
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
}
