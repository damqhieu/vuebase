import {
  SET_USER
} from '../mutation-types'

/**
 * state
 */
const state = {
  user: {}
}

/**
 * actions
 */
const actions = {
  setUser ({ commit }, payload) {
    let { user } = payload
    commit(SET_USER, user)
  },
  getUser ({commit, dispatch }, payload) {
    let { user_id, params } = payload
    dispatch(
      'fetchApi',
      {
        url: `users/${user_id}`,
        method: 'GET',
        params: params || {},
        success: (response) => {
          commit(SET_USER, response.data)
        }
      },
      { root: true}
    )
  },
  createUser ({commit, dispatch}, payload) {
    let { user, cb } = payload
    dispatch('fetchApi', {
      url: 'users',
      method: 'POST',
      data: user,
      success: cb
    }, {root: true})
  },
  updateUser ({commit, dispatch}, payload) {
    let { id, user, cb } = payload
    dispatch('fetchApi', {
      url: `users/${id}`,
      method: 'PUT',
      data: user,
      success: cb
    }, {root: true})
  }
}

/**
 * mutations
 */
const mutations = {
  [SET_USER]: (state, user) => {
    state.user = user
  }
}

/**
 * getters
 */
const getters = {
  userDetail: (state) => state.user
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
}
