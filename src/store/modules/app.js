import {
  SET_DRAWER,
  SET_MINI_DRAWER
} from '../mutation-types'

/**
 * state
 */
const state = {
  drawer: true,
  miniDrawer: false
}

/**
 * actions
 */
const actions = {
  setDrawer ({ commit }, payload) {
    commit(SET_DRAWER, !!payload)
  },
  setMiniDrawer ({ commit }, payload) {
    commit(SET_MINI_DRAWER, !!payload)
  }
}

/**
 * mutations
 */
const mutations = {
  [SET_DRAWER]: (state, drawer) => {
    state.drawer = drawer
  },
  [SET_MINI_DRAWER]: (state, miniDrawer) => {
    state.miniDrawer = miniDrawer
  }
}

/**
 * getters
 */
const getters = {
  drawerModel: (state) => state.drawer,
  miniDrawer: (state) => state.miniDrawer
}

export default {
  state,
  actions,
  mutations,
  getters
}
