import axios from '@/plugins/axios.js'
import router from '@/router'
import {
  FETCHING_RESOURCES,
  FETCHING_RESOURCES_DONE,
  FETCHING_RESOURCES_FAIL
} from '../mutation-types'
import { forIn } from 'lodash'

/**
 * state
 */
const state = {
  fetchApi: false,
  apiErrors: null
}

/**
 * actions
 */
const actions = {
  async fetchApi ({ commit, dispatch }, payload = {}) {
    let { success, error, showWarnings } = payload
    showWarnings = showWarnings === undefined ? true : showWarnings

    try {
      commit(FETCHING_RESOURCES, true)
      let response = await axios.request(payload)
      commit(FETCHING_RESOURCES_DONE)
      success && success(response.data)
    } catch (e) {
      let err = null
      if (e.response) {
        if (showWarnings) {
          switch (e.response.status) {
            case 422:
              let msg = []
              forIn(e.response.data.data.errors, (err, idx) => {
                msg.push('&bullet; ' + err[0])
              })
              dispatch('showNotify', {text: msg.join('<br />'), color: 'warning'})
              break
            case 403:
              dispatch('showNotify', {text: 'Bạn không có quyền thực hiện tác vụ này', color: 'warning'})
              router.push({name: 'forbidden'})
              break
            case 401:
              dispatch('showNotify', {text: 'Phiên đăng nhập hết hạn vui lòng đăng nhập', color: 'warning'})
              dispatch('clearLogged')
              router.push({name: 'login'})
              break
            case 500:
              dispatch('showNotify', {text: 'Máy chủ sảy ra sự cố vui lòng thử lại sau', color: 'error'})
              router.push({name: 'opps'})
              break
          }
        }
        err = e.response
      } else if (e.request) {
        err = e.request
      } else {
        err = e.message
      }
      commit(FETCHING_RESOURCES_FAIL, err)
      error && error(err)
    } finally {
      commit(FETCHING_RESOURCES, false)
    }
  }
}

/**
 * mutations
 */
const mutations = {
  [FETCHING_RESOURCES_FAIL]: (state, err) => {
    state.apiErrors = err.response
  },
  [FETCHING_RESOURCES_DONE]: (state) => {
    state.apiErrors = null
  },
  [FETCHING_RESOURCES]: (state, isFechApi) => {
    state.fetchApi = isFechApi
  }
}

/**
 * getters
 */
const getters = {
  allApiErrors: (state) => state.apiErrors,
  isFetchingApi: (state) => state.fetchApi
}

export default {
  state,
  actions,
  mutations,
  getters
}
