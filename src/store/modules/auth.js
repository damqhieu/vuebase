import ls from 'local-storage'
import {
  SET_LOGGED
} from '../mutation-types'

/**
 * state
 */
const state = {
  logged: false
}

/**
 * actions
 */
const actions = {
  checkLogged ({ commit }, payload) {
    let auth = ls('auth') || {}
    commit(SET_LOGGED, !!auth.token)
  },
  setLogged ({commit}, payload) {
    payload = payload || {}
    ls('auth', payload)
    commit(SET_LOGGED, !!payload.token)
  },
  clearLogged ({commit}, payload) {
    ls.remove('auth')
    commit(SET_LOGGED, false)
  }
}

/**
 * mutations
 */
const mutations = {
  [SET_LOGGED]: (state, logged) => {
    state.logged = logged
  }
}

/**
 * getters
 */
const getters = {
  isLogged: (state) => state.logged
}

export default {
  state,
  actions,
  mutations,
  getters
}
