// axios fetch data state
export const FETCHING_RESOURCES_DONE = 'API/FETCHING_RESOURCES_DONE'
export const FETCHING_RESOURCES_FAIL = 'API/FETCHING_RESOURCES_FAIL'
export const FETCHING_RESOURCES = 'API/FETCHING_RESOURCES'
// app
export const SET_DRAWER = 'DRAWER/SET_DRAWER'
export const SET_MINI_DRAWER = 'DRAWER/SET_MINI_DRAWER'
// notify
export const NOTIFY_INSTANCE = 'NOTIFY/INSTANCE'
export const NOTIFY_TIME = 'NOTIFY/TIME'
export const NOTIFY_TEXT = 'NOTIFY/TEXT'
export const NOTIFY_COLOR = 'NOTIFY/COLOR'
// auth
export const SET_LOGGED = 'AUTH/SET_LOGGED'
// breadcrumb
export const SET_BREADCRUMB = 'BREADCRUMB/SET_BREADCRUMB'
// data views
export const SET_DATA_VIEW = 'DATA_VIEW/SET_DATA_VIEW'
export const UPDATE_DATA_VIEW_ENTRY = 'DATA_VIEW/UPDATE_DATA_VIEW_ENTRY'
// data views
export const SET_USER = 'USER/SET_USER'
